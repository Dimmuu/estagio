## Estou utilizando a API xlrd do python
import xlrd
## Tambem precisei usar a Pandas para converter o arquivo csv em xlsx
## Só assim posso lê-lo (Observações no final)
## import pandas as pd
#read_file = pd.read_csv (r'C:\Users\guilh\Documents\estagio\Tarefa1\mapa.csv')
#read_file.to_excel (r'C:\Users\guilh\Documents\estagio\Tarefa1\mapa.xlsx', index = None, header=True)
import xlsxwriter
##Método bubblesort
def bubbleSort(Lista,Lista2):
    for i in range(0,len(Lista)-1):
        for j in range(0,len(Lista)-1-i):
            if(Lista[j]>Lista[j+1]):
                Lista[j],Lista[j+1]=Lista[j+1],Lista[j]
                Lista2[j],Lista2[j+1]=Lista2[j+1],Lista2[j]
    return Lista,Lista2
## Esse path assume que o arquivo excel esteja na mesma pasta do programa
path="mapa.xlsx"
inputWorkbook=xlrd.open_workbook(path)
## pegando a folha 0 do arquivo
inputWorksheet=inputWorkbook.sheet_by_index(0)
## Criação do novo arquivo Excel
outWorkbook=xlsxwriter.Workbook("resultado.xlsx")
outSheet=outWorkbook.add_worksheet()
## Essas linhas fazem o cabeçalho da tabela
outSheet.write("A1",inputWorksheet.cell_value(0,0))
outSheet.write("B1",inputWorksheet.cell_value(0,1))
## Dessa vez arquivamos os valores das duas colunas para poder organizá-las dentro
## do bubblesort
nomes=[]
populacoes=[]
for item in range(1,inputWorksheet.nrows):
    nomes.append(inputWorksheet.cell_value(item,0))
    populacoes.append(inputWorksheet.cell_value(item,1))
bubbleSort(populacoes,nomes)
## Nesse For a tabela e preenchida com os valores da antiga multiplicando-se
## por 2 o valor da população
for item in range(1,inputWorksheet.nrows):
    outSheet.write(item,0,nomes[item-1])
    outSheet.write(item,1,populacoes[item-1])
## Fechasse o arquivo novo criado
outWorkbook.close()

### OBSERVAÇÕES ###
## O Panda faz uma conversão meio problemática e não gosta de ser chamado mais
## de uma vez por isso o deixei comentado(depois volto nisso)



