##aqui eu precisei da biblioteca xlrd e da pycep_correios
import xlrd
import pycep_correios
from pycep_correios import get_cep_from_address
from pycep_correios.exceptions import BaseException

#import pandas as pd
#read_file = pd.read_csv (r'C:\Users\guilh\Documents\estagio\Tarefa3\CEPs.csv')
#read_file.to_excel (r'C:\Users\guilh\Documents\estagio\Tarefa3\CEPs.xlsx', index = None, header=True)


## Esse path assume que o arquivo excel esteja na mesma pasta do programa
path="CEPs.xlsx"
inputWorkbook=xlrd.open_workbook(path)
## pegando a folha 0 do arquivo
inputWorksheet=inputWorkbook.sheet_by_index(0)
##recupero os ceps no arquivo CEPs convertido
ceps=[]
enderecos=[]
cep2=[]
##nesse for é necessário converter primeiramente o valor que vem do excel
##que está em float para inte so depois colocá-lo no vetor ceps como uma string
##porque a biblioteca so aceita strings
for y in range(1,inputWorksheet.nrows):
    aux=int(inputWorksheet.cell_value(y,0))
    ceps.append(str(aux))
print(ceps)
##a problemática começa na versão limitada do pycep_correios
##que so me oferece algumas partes do cep 

endereco = pycep_correios.consultar_cep(ceps[0])

##com a função get_cep_from_adress eu consigo mais resultados
##entretanto nem todos os valores das tabelas apresentadas
##podem ser lidos pela biblioteca, provavelmente porque durante a conversão
##alguns dos valores perdem o valor 0 que está na frente o tornando
##inválidos
try:
    cep2 = pycep_correios.get_cep_from_address(
        state=endereco['uf'], city=endereco['cidade'], street=endereco['end'])
    
except BaseException as exc:
    print(exec.message)

print("ibge: "+cep2[0]['ibge'])
print("localidade: "+cep2[0]['localidade'])
print("logradouro: "+cep2[0]['logradouro'])
print("bairro: "+cep2[0]['bairro'])
print("complemento: "+cep2[0]['complemento'])
print("uf: "+cep2[0]['uf'])
print("cep: "+cep2[0]['cep'])
print("gia: "+cep2[0]['gia'])
print("unidade: "+cep2[0]['unidade'])
